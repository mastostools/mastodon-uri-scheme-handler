## Demo

A functioning demo of this web app is running at https://mastoURIhandler.surge.sh

## Summary

This web app allows users to open up special links in their web browser that begin with "web+apub:" into their Mastodon instance's web client

### Syntax

Links in the following format will load the referenced user's profile in your chosen instance's web client:
```
web+apub:@user@instance
```

Links in the following format will load the referenced post in your chosen instance's web client:
```
web+apub:@user@instance/postID
```

### Example of usage

- Add a link to your profile on your website

**HTML:**
```
<a href="web+apub:@me@mastodon.social">Follow Me on Mastodon!</a>
```

**Markdown:**
```
[Follow Me on Mastodon!](web+apub:@me@mastodon.social)
```
    
## Privacy
localStorage is used to store the name of your instance to your web browser.
This data lives only in your browser and won't be sent anywhere else.
    
## Requirements
- Make sure you are logged in to your instance
- Currently confirmed to work on: Chrome and Firefox (on desktops only)
- Also only currently tested to work with the core Mastodon client
- Javascript must be enabled

## TODO
- Add regex to filter out unsupported link formats
- Test and add support for more clients
- Test and add support for more web browsers

## Known Bugs
- If you set this to work on Chrome before setting it on Firefox, then it won't work on Firefox 🤷 ( [#1](https://gitlab.com/mastostools/mastodon-uri-scheme-handler/-/issues/1) )

## Issue Tracker

Please report any other issues you experience with this application at the following link

https://gitlab.com/mastostools/mastodon-uri-scheme-handler/-/issues

## Future Extension Possibilities

- Chrome Extension
- Android app with bare bones features to demo app implementation
- Patch for Mastodon clients to add support and set up handlers directly
- Figure out a way of detecting whether someone has support for the protocol installed